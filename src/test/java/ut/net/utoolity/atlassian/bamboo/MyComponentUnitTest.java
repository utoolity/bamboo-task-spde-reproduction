package ut.net.utoolity.atlassian.bamboo;

import org.junit.Test;
import net.utoolity.atlassian.bamboo.MyPluginComponent;
import net.utoolity.atlassian.bamboo.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}