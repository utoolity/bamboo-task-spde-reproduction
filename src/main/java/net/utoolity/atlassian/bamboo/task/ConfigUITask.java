package net.utoolity.atlassian.bamboo.task;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;

public class ConfigUITask implements CommonTaskType
{
    @Override
    @NotNull
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException
    {
        final BuildLogger buildLogger = commonTaskContext.getBuildLogger();
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(commonTaskContext);

        final String url = commonTaskContext.getConfigurationMap().get("url");

        buildLogger.addBuildLogEntry("Executing ConfigUITask ...");
        buildLogger.addBuildLogEntry("... which currently only logs the URL passed from configuration: "
                + url);

        return taskResultBuilder.success().build();
    }
}
