package net.utoolity.atlassian.bamboo.task;

import java.util.HashMap;
import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.task.RuntimeTaskDataProvider;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;

public class DemoRuntimeTaskDataProvider implements RuntimeTaskDataProvider
{
    private static final String URL = "url";

    private ApplicationProperties applicationProperties;

    public void setApplicationProperties(final ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    @Override
    @NotNull
    public Map<String, String> populateRuntimeTaskData(@NotNull TaskDefinition taskDefinition,
            @NotNull CommonContext commonContext)
    {
        Map<String, String> data = new HashMap<String, String>();

        data.put(URL, applicationProperties.getBaseUrl(UrlMode.ABSOLUTE));

        return data;
}

    @Override
    public void processRuntimeTaskData(@NotNull TaskDefinition taskDefinition, @NotNull CommonContext commonContext)
    {
        // NOTE: Intentionally doing nothing
    }
}
