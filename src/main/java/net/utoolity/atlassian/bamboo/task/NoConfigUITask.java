package net.utoolity.atlassian.bamboo.task;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.sal.api.ApplicationProperties;

public class NoConfigUITask implements CommonTaskType
{
    private final ApplicationProperties applicationProperties;

    public NoConfigUITask(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    @SuppressWarnings("deprecation")
    @Override
    @NotNull
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException
    {
        final BuildLogger buildLogger = commonTaskContext.getBuildLogger();
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(commonTaskContext);

        buildLogger.addBuildLogEntry("Executing NoConfigUITask ...");
        buildLogger.addBuildLogEntry("... which currently only logs the application base URL: "
                + applicationProperties.getBaseUrl());

        return taskResultBuilder.success().build();
    }
}
