package net.utoolity.atlassian.bamboo.task;

import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;

public class RuntimeTaskDataProviderTask implements CommonTaskType
{
    @Override
    @NotNull
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException
    {
        final BuildLogger buildLogger = commonTaskContext.getBuildLogger();
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(commonTaskContext);

        Map<String, String> runtimeTaskContext = commonTaskContext.getRuntimeTaskContext();
        final String url = runtimeTaskContext.get("url");

        buildLogger.addBuildLogEntry("Executing RuntimeTaskDataProviderTask ...");
        buildLogger.addBuildLogEntry("... which currently only logs the URL passed by the RuntimeTaskDataProvider: "
                + url);

        return taskResultBuilder.success().build();
    }
}
