# Overview

Minimal Bamboo plugin to reproduce ServiceProxyDestroyedException problems in context of Task Plugin Module implementations.

## What it does

Implements Tasks that simply write some output to the build log when executed, using a dependency injected component from the host product in different ways to provoke the ServiceProxyDestroyedException after a plugin diable/enable cycle.

Apart from reproducing the issue, it can also be used to test possible workarounds.

## Reproduction steps

This is for developer testing and demonstration purposes, hence only installable via explicit build, e.g. clone repository and start via Atlassian Plugin SDK commands:

* Preliminaries
    * Clone repository
    * Checkout master
    * Start via Atlassian Plugin SDK, e.g. {{atlas-run}}
    * Add/configure a plan (using existing example plans is ok as well)
* Repro case for TaskConfigurator usage
    * Add the "A Task with config UI" task (does not need to be saved, just opening the task configuration screen once is enough)
    * Disable the plugin via UPM
    * Enable the plugin via UPM
    * Try to add another ""A Task with config UI" " task - this will trigger the exception directly, the task configuration screen will not open.
* Repro case for RuntimeTaskDataProvider usage
    * Add the "A Task using a RuntimeTaskDataProvider" task (needs to be saved, as error only occurs on task execution)
    * Run plan once (expect no failure, log should show the baseUrl)
    * Disable the plugin via UPM
    * Enable the plugin via UPM
    * Run plan again - this will trigger the exception directly, the build will not even start due to fatal error in form of ServiceProxyDestroyedException.

NOTE: The plugin also implements "A Task without config UI" - this uses an injected ApplicationProperties instance as well, but does *not* trigger the exception.
